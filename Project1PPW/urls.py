from django.contrib import admin
from django.urls import path, include

from . import views
from caridok.views import listPesanan
from bmi.views import  bmi_func

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.homepage, name = 'homepage'),
    path('feedback/', include('feedback.urls')),
    path('cariDok/', include('caridok.urls')),
    path('cariDok/',include('buatDok.urls')),
    path('tanyadok/', include('tanyadok.urls')),
    path('listPesanan/', listPesanan, name='listPesanan'),
    path('accounts/', include('user.urls')),
    path('berita/', include('berita.urls')),
    path('bmi/',bmi_func, name='BMIPage'),
    path('penyakit/', include('penyakit.urls')),
]

