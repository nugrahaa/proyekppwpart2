from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

def homepage(request):
    if request.user.is_authenticated:
        status = 1
    else:
        status = 0
    context = {
        'judul' : 'Dokterin - Solusi Kesehatan',
        'status' : status
    }

    return render(request, 'homepage.html', context)