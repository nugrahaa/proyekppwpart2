# Project 1 PPW
[![pipeline status](https://gitlab.com/nugrahaa/project1ppw/badges/master/pipeline.svg)](https://gitlab.com/nugrahaa/project1ppw/commits/master)
[![coverage report](https://gitlab.com/nugrahaa/project1ppw/badges/master/coverage.svg)](https://gitlab.com/nugrahaa/project1ppw/commits/master)

## Developers Guide

### Development Team

- Ari Angga Nugraha --> **Homepage + Feedback + Berita** 
- Muhammad Fadhlan --> **Tanya Dok**
- Muhammad Fathurizki Herlando --> **Buat Dok + BMI(Kalori)**
- Muhammad Ilham Al-Ghifari --> **Cari Dok + Penyakit**
- all : **User**

**Link Herokuapp :**
> dokterin.herokuapp.com

**Application Detail**:

Dokterin adalah sebuah platform website yang bertujuan untuk memudahkan masyarakat terutama mahasiswa dalam membuat perjanjian konsultasi dengan dokter untuk menyelesaikan masalah kesehatan mereka. Harapan dari website ini pada Industri 4.0 adalah website ini dapat melakukan penyajian diagnosis secara lebih akurat dengan analisis form dan data dari smart watch, dan smart bed yang dapat terus memonitor kesehatan kita. Melalui kedua alat tersebut diharapkan dapat ditemukan dokter yang tepat dengan jarak sedekat mungkin dari posisi kita saat itu. Selain itu data dari smart watch yang terus diintegrasikan dengan website khusus untuk pasien dengan penyakit rawan kambuh dan akan segera memberikan sinyal bahaya kepada website bila terjadi sesuatu yang tidak biasa pada pasien. Website akan secara otomatis memberitahukan rumah sakit terdekat mengenai posisi dan status pasien. Namun, untuk sementara ini, karena keterbatasan kemampuan dan waktu, maka kami akan berfokus terlebih dahulu untuk membuat website perjanjian konsultasi dokter, yang memang menjadi ide awal kami terlebih dahulu.

**Daftar Fitur :**

1. Pencarian dokter sesuai dengan penyakit 
2. Halaman profil dokter
3. Booking konsultasi dokter sesuai tarif yang diberikan
4. History pemesanan
5. Pencarian Dokter sesuai dengan nama Dokter (optional)



