$(document).ready(function() {

    $body = $("body");

    $.ajax({
        url: "https://newsapi.org/v2/top-headlines?country=id&category=health&apiKey=230fd988de08429c81a3e27c4934058b",
        dataType: 'json',

        success: function(data) {

            var results = document.getElementById("results");
            console.log(data);

            console.log(data.totalResults);

            console.log(data.articles[0].author);

            renderBerita(data);

        },

        type: 'GET',
    })

    $(".daftar-berita-container").hover(function() {
        $("body").css('background-color', '#C7ECEE').fadeIn('slow');
    })

    function renderBerita(data) {
        if (data.totalResults == 0) {
            results.innerHTML += "<h3>Maaf berita tidak ditemukan :(</h3>";
        } else {
            console.log(data.totalResults);

            for (i = 0; i < data.articles.length; i++) {
                results.innerHTML +=
                    "<div class='berita-card'>" +
                    "<img src='" + data.articles[i].urlToImage + "' alt='foto-berita'>" +
                    "<div class='card-text'>" +
                    "<h3>" + data.articles[i].title + "</h3>" +
                    "<p>" + data.articles[i].description + "<br>" +
                    "<a href='" + data.articles[i].url + "' target='_blank'>baca lebih lanjut<a>" +
                    "<p>" +
                    "</div>" +
                    "</div>";
            }
        }
    }



    $("#button").click(function() {
        var keyword = $("#search").val();

        $("#results").empty();

        $.ajax({
            url: "https://newsapi.org/v2/everything?q=" + keyword + "&apiKey=230fd988de08429c81a3e27c4934058b",
            dataType: 'json',

            success: function(data) {

                var results = document.getElementById("results");
                console.log(data);

                console.log(data.totalResults);

                console.log(data.articles[0].author);

                renderBerita(data);

            },

            type: 'POST',
        })
    })

});