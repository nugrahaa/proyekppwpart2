from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from .views import berita
from django.http import HttpRequest
from datetime import date
import unittest


# Create your tests here.
class PepewStatusUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_berita_list_is_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code,200)

    def test_invalid_url_not_found(self) :
        response = Client().get('/feedback/ariganteng/')
        self.assertEqual(response.status_code, 404)

    # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    def test_berita_page_using_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, berita)

    def test_berita_list_page_uses_template(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'berita.html')