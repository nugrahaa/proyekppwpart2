from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout

def berita(request):
    if request.user.is_authenticated:
        status = 1
    else :
        status = 0

    context = {
        'status' : status
    }
    
    return render(request, 'berita.html', context)