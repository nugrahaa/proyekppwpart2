from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from django.contrib.auth.models import User
from .views import bmi_func
from django.http import HttpRequest
from datetime import date
import unittest

# Create your tests here.
class BmiUnitTest(TestCase):
    
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_BMI_list_is_exist(self):
        response = Client().get('/bmi/')
        self.assertEqual(response.status_code,302)

    def test_BMI_url_not_found(self) :
        response = Client().get('/bmn/')
        self.assertEqual(response.status_code, 404)

    # - - - - - - - - Function TEST - - - - - - - - -
    def test_BMI_Page_using_func(self):
        found = resolve('/bmi/')
        self.assertEqual(found.func, bmi_func)

    # - - - - - - - - Template TEST - - - - - - - - -
    def test_BMI_page_uses_template(self):
        user = User.objects.create(username='testuser')
        user.set_password('secret')
        user.save()
        self.client.login(username = 'testuser',password= 'secret')
        response = self.client.get('/bmi/')
        self.assertTemplateUsed(response, 'bmi.html')