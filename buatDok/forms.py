from django import forms
from .models import profilDok
from datetime import datetime

class buatDokForm(forms.ModelForm):
    class Meta:
        model = profilDok
        fields = '__all__'
        widgets = {
            'nama' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),

            'umur' : forms.NumberInput(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),

            'jenis_kelamin' : forms.Select(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),

            'spesialis' : forms.Select(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),

            'pengalaman' : forms.NumberInput(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),

            'tempatPraktek' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),
            'day' : forms.Select(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA'
                }
            ),
            'waktuAwal' : forms.TimeInput(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA',
                    'placeholder': 'Format: HH:MM'
                }
            ),
            'waktuAkhir' : forms.TimeInput(
                attrs={
                    'class':'form-control',
                    'background-color': '#C7ECEE',
                    'color': '#909DAA',
                    'placeholder': 'Format: HH:MM'
                }
            )
        }