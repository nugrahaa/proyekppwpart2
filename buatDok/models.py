from django.db import models

# Create your models here.
class profilDok(models.Model):    
    Spec_choices = (
        ('Anak','Anak'),
        ('THT','THT'),
        ('Umum','Umum'),
        ('Penyakit Dalam','Penyakit Dalam')
    )
    Day_choices = (
        ('Senin','Senin'),
        ('Selasa','Selasa'),
        ('Rabu','Rabu'),
        ('Kamis','Kamis'),
        ('Jumat','Jumat'),
        ('Sabtu','Sabtu'),
        ('Minggu','Minggu')
    )
    JK_choices = (('Laki-laki','Laki-laki'),('Perempuan','Perempuan'))
    nama = models.CharField(max_length=50)
    umur = models.IntegerField(default='0')
    jenis_kelamin = models.CharField(
        max_length = 100,
        choices = JK_choices,
        default = 'Perempuan'
        )
    spesialis = models.CharField(        
        max_length = 100,
        choices = Spec_choices,
        default = 'Umum'
        )
    pengalaman = models.IntegerField(blank=False,default='0')
    tempatPraktek = models.CharField(max_length=100)
    day = models.CharField(
        max_length=40,
        choices = Day_choices,
        default = 'Senin'
        )
    waktuAwal = models.TimeField(default= '08:00')
    waktuAkhir = models.TimeField(default = '21:00')

    def __str__(self):
        return self.nama