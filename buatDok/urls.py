from django.urls import path
from . import views

urlpatterns = [
    path('create/',views.buat_dok.as_view(success_url='/cariDok/'), name='create-Dok'),
    path('<str:spesialis>/',views.list_dok.as_view(),name= 'list-Dok')
]