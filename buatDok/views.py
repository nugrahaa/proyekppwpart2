from django.shortcuts import render,redirect, get_object_or_404
from .models import profilDok
from .forms import buatDokForm
from django.forms import formset_factory
from django.views.generic import ListView, CreateView
from tanyadok.models import PertanyaanModel

# Create your views here.
class buat_dok(CreateView):
    model = profilDok
    form_class = buatDokForm
    template_name = 'buat_dok.html'
    context = 'form'

class list_dok(ListView):
    model = profilDok
    context_obj_name = 'List Dokter'
    template_name = "list_dok.html" 

    def get_queryset(self):
        return profilDok.objects.filter(spesialis=self.kwargs['spesialis'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['spesialis'] = self.kwargs['spesialis']
        context['object_list2'] = PertanyaanModel.objects.order_by('-id')[:4]
        return context                          
#def list_dok(request,spesialis):
    #if tanyaDok.objects.count()<=6:
    #   qs2 = tanyaDok.objects.all()
    #else:
    #    qs2 = tanyaDok.objects.order_by('-timestamp')[:6]
    #qs = profilDok.objects.filter(spesialis=spesialis)
    #qs3 = WaktuDok.objects.all()
    #template_name = 'list_dok.html'
    #context = {'object_list':qs,'object_list2':qs2}
    #context = {'object_list':qs, 'object_list3':qs3}
    #return render(request,template_name,context)