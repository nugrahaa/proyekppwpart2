from django.apps import AppConfig


class CaridokConfig(AppConfig):
    name = 'caridok'
