from django.db import models

# Create your models here.
class PemesananModel(models.Model): 
    nama_pemesan    = models.CharField(max_length = 20)
    no_hp           = models.CharField(max_length = 20)
    alamat          = models.CharField(max_length = 20)
    pilihan_waktu   = models.CharField(max_length = 20)

    def __str__(self):
        return self.nama_pemesan