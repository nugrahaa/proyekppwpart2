from django import  forms
from .models import FeedBackModel


class FeedBackForm(forms.ModelForm):
    class Meta:
        model = FeedBackModel
        fields = [
            'nama_pengirim',
            'jenis_kelamin',
            'komentar',
            'rating'
        ]

        widgets = {
            'nama_pengirim' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'isi nama dengan namamu (boleh anonymous)'
                }
            ),

            'jenis_kelamin' : forms.Select (
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'komentar' : forms.Textarea (
                attrs = {
                    'class' : 'form-control',
                }
            ),

            'tanggal' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'type' : 'date',
                }
            ),

            'rating' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'Beri kami rating 1-9'
                }
            )
        }


