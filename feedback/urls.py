from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('list/', views.list, name="list-feedback"),
    path('create/', views.create, name="create-feedback"),
]
