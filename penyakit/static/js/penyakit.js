$(document).ready(function() {
    $("#results").empty();

    $.ajax({
        async: true,
        type: 'GET',
        url: "https://clinicaltables.nlm.nih.gov/api/conditions/v3/search?terms=cough",
        dataType: 'json',
        success: function(searchRes) {
            let penyakitArray = searchRes;
            let namaPenyakitArray = penyakitArray[3];
            if (penyakitArray[3].length == 0) {
                results.innerHTML += "<h3> Maaf penyakit yang kamu cari tidak ditemukan</h3>"
            } else {
                $('#results').empty();
                for (i = 0; i < penyakitArray[3].length; i++) {
                    results.innerHTML +=
                        "<div class='berita-card'>" +
                        "<div class='card-text'>" +
                        "<h3>" + namaPenyakitArray[i] + "</h3>" +
                        "<p>" +
                        "</div>" +
                        "</div>";
                }
                console.log(penyakitArray)
            }
        },

        type: 'GET',
    })

    $('#form-inline').submit(function(event) {
        let searchItem = $('#input').val();
        let urlItem = "https://clinicaltables.nlm.nih.gov/api/conditions/v3/search?terms=" + searchItem;

        $.ajax({
            async: true,
            type: 'GET',
            url: urlItem,
            dataType: 'json',
            success: function(searchRes) {
                let penyakitArray = searchRes;
                let namaPenyakitArray = penyakitArray[3];
                
                $('#results').empty();
                if (penyakitArray[3].length == 0) {
                    results.innerHTML += "<h3> Maaf penyakit yang kamu cari tidak ditemukan</h3>"
                } else {
                    $('#results').empty();
                    for (i = 0; i < penyakitArray[3].length; i++) {
                        results.innerHTML +=
                            "<div class='berita-card'>" +
                            "<div class='card-text'>" +
                            "<h3>" + namaPenyakitArray[i] + "</h3>" +
                            "<p>" +
                            "</div>" +
                            "</div>";
                    }
                    console.log(penyakitArray)
                }
            },

            type: 'GET',
        })
        event.preventDefault();
    })

});