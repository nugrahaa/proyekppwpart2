from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.http import HttpRequest
from datetime import datetime
from django.contrib.auth.models import User
from .views import penyakit_view

# Create your tests here.
class PenyakitAppUnitTest(TestCase):
    # - - - - - - - - - - URL TEST - - - - - - - -
    def test_penyakit_page_is_exist(self):
        response = Client().get('/penyakit/')
        self.assertEqual(response.status_code,302)

    def test_invalid_url_not_found(self) :
        response = Client().get('/ablebleeehhh/')
        self.assertEqual(response.status_code, 404)

     # - - - - - - - - TEMPLATE TEST - - - - - - - - -
    # def test_penyakit_page_using_func(self):
    #     found = resolve('/penyakit/')
    #     self.assertEqual(found.func, penyakit_view)

    def test_penyakit_page_uses_template(self):
        user = User.objects.create(username='testuser')
        user.set_password('secret')
        user.save()
        self.client.login(username = 'testuser',password= 'secret')
        response = self.client.get('/penyakit/')
        self.assertTemplateUsed(response, 'penyakit.html')