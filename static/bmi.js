$(document).ready(function() {

    $('#calory_button').click(function(){

        var makanan = $('#makanan_input').val();
        var hasil = $('#results');
        hasil.empty();
        
        $.ajax({
            async: true ,
            crossDomain : true,
            url: "https://edamam-food-and-grocery-database.p.rapidapi.com/parser?ingr="+makanan,
            method: "GET",
            headers: {
                "x-rapidapi-host": "edamam-food-and-grocery-database.p.rapidapi.com",
                "x-rapidapi-key": "2ad7ddd2bbmsh1b26558ff819430p1b7aaejsn054f05daaa18"
            },
            success: function(response){
                var results = document.getElementById("results");
                if (response.hints.length == 0) {
                    results.innerHTML += "<h3>Maaf kayaknya itu bukan makanan(</h3>";
                } else {
                    for (i = 0; i < response.hints.length; i++) {
                        var temp = i+1;
                        results.innerHTML +=
                            "<tr>" + 
                            "<th scope='row'>"+ temp + "</th>" +
                            "<td>" + response.hints[i].food.category + "</td>" +
                            "<td>" + response.hints[i].food.label + "</td>" +
                            "<td>"  + response.hints[i].food.nutrients.ENERC_KCAL + "</td>" +
                            "</tr>";
                    }
                }
            } 
        });
    })

    $("#myBtn").hover(function(){
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    })

    $(window).scroll(function (event) {
        mybutton = document.getElementById("myBtn");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
        // Do something
    });
});