from .models import PertanyaanModel
from django import forms
class PertanyaanForm(forms.ModelForm):
    
    class Meta:
        model = PertanyaanModel
        fields = [
            'nama_penanya',
            'jenis_penyakit',
            'pertanyaan'
        ]

        widgets = {
            'nama_penanya' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'isi nama anda'
                }
            ),

            'jenis_penyakit' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'penyakit yang anda tanya'
                }
            ),

            'pertanyaan' : forms.TextInput (
                attrs = {
                    'class' : 'form-control',
                    'placeholder' : 'pertanaan anda?'
                }
            )
        }


