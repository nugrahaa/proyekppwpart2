from django.db import models

class PertanyaanModel(models.Model):
    nama_penanya        = models.CharField(max_length=50)
    jenis_penyakit      = models.CharField(max_length=50)
    pertanyaan          = models.CharField(max_length=255)

    def __str__(self):
        return self.nama_penanya
    
