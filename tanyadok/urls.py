from django.urls import path

from . import views

urlpatterns = [
    path('list/', views.list, name="list-pertanyaan"),
    path('tanya/', views.create, name="buat-pertanyaan"),
]
