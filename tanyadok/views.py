from django.shortcuts import render, redirect

from .models import PertanyaanModel
from .forms import PertanyaanForm

# Create your views here.
def list(request):
    pertanyaans = PertanyaanModel.objects.all()
    context = {
        'judul' : 'Tanya Dokter',
        'pertanyaans' : pertanyaans
    }

    return render(request, 'list_tanya.html', context)

def create(request):
    pertanyaan_form = PertanyaanForm(request.POST or None)

    if request.method == "POST" :
        if pertanyaan_form.is_valid() :
            pertanyaan_form.save()

            return redirect('list-pertanyaan')

    context = {
        'judul' : 'Create Feedback',
        'pertanyaan_form' : pertanyaan_form
    }

    return render(request, 'tanya.html', context)